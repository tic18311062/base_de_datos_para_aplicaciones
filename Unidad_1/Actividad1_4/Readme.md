# actividad 1.4

#### scrip para crear la tablas con los forentkey
-- MySQL Workbench Forward Engineering
-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `mydb` ;

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`CIENTIFICOS`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`CIENTIFICOS` ;

CREATE TABLE IF NOT EXISTS `mydb`.`CIENTIFICOS` (
  `DNI` VARCHAR(8) NOT NULL,
  `NomApels` NVARCHAR(255) NULL,
  PRIMARY KEY (`DNI`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`PROYECTO`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`PROYECTO` ;

CREATE TABLE IF NOT EXISTS `mydb`.`PROYECTO` (
  `id` CHAR(4) NOT NULL,
  `nombres` NVARCHAR(255) NULL,
  `horas` INT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Asignacion_A`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Asignacion_A` ;

CREATE TABLE IF NOT EXISTS `mydb`.`Asignacion_A` (
  `cientifico` VARCHAR(8) NOT NULL,
  `proyecto` CHAR(4) NOT NULL,
  PRIMARY KEY (`cientifico`, `proyecto`),
  INDEX `FK2_idx` (`proyecto` ASC) VISIBLE,
  CONSTRAINT `FK1`
    FOREIGN KEY (`cientifico`)
    REFERENCES `mydb`.`CIENTIFICOS` (`DNI`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK2`
    FOREIGN KEY (`proyecto`)
    REFERENCES `mydb`.`PROYECTO` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

####consultas con subconsultas
a
#####con inner join

use cientificos;
SELECT DNI,NomApels,id,Nombres FROM cientificos C 
INNER JOIN asignado_a A ON C.DNI=A.Cientifico
INNER JOIN proyecto P ON P.id=A.proyecto;

#####sin inner join:
USE cientificos;
SELECT DNI,NomApels,id,Nombres FROM cientificos C,asignado_a A,proyecto P WHERE C.DNI = A.Cientifico AND A.proyecto=p.id; 


#####con sub consultas:

DESCRIBE SELECT DNI,NomApels FROM cientificos WHERE 1 <
(SELECT COUNT(*) FROM asignado_a WHERE cientifico=cientificos.DNI)
AND 80 <
(SELECT AVG(Horas) FROM proyectos INNER JOIN asignado_a ON proyectos.id = asignado_a.proyecto WHERE cientifico = cientificos.DNI);
![querry](https://gitlab.com/tic18311062/base_de_datos_para_aplicaciones/raw/master/Unidad_1/Actividad1_4/img/querry.PNG)

