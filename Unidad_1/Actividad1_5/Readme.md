### Que es un trigger?
¿Que es un Trigger(Disparador)?

Los triggers son bloques de código SQL asociados a una tabla y que tambien  se ejecutan 
automáticamente como reacción a una operación como el INSERT, UPDATE o DELETE
sobre la dicha tabla.

Un trigger esta fija a la tabla que fue hecha.
para quitar el trigger usamos DROP TRIGGER [nombre]

Existe el BEFORE y AFTER que practicamente que dice que el trigger se ejecuta ANTES o Despues de un comando.


Ventajas
    Creando triggers hace que el trabajo del servidor sea menor, ya que vas a tener condiciones para el comando que se
    va a ejecutar.
Desventajas
    Mucho mas mantenimiento del gestor que estes utilizando. Para mover a diferentes gestores
    es un mayor esfuerzo.
-------------------------------------------------------------------------------
BEGIN
TRIGGER `log` 
BEFORE UPDATE ON `users` 
    FOR EACH ROW 
    INSERT INTO bitacora(fecha_hora) VALUES(now());
END
BEGIN
--------------------------------------------------------------------------------
BEGIN
INSERT 'nombre_usr'
BEFORE INSERT ON 'users'
DECLARE nombre_tamano int;
SET nombre_tamano = (SELECT LENGTH(new.nombre));
	IF nombre_tamano < 5 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Nombre no valido';
	END IF;
END
--------------------------------------------------------------------------------
BEGIN 
TRIGGER 'insert_log'
DECLARE insert_tamano int;
SET insert_tamano = (SELECT ROW_COUNT());
    IF insert_taman >= 3 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'no puedes insertar mas de 2 usuarios';
    END IF;
END

-----------------------------------
Software utilizado: heidiSQL,10.3.14-MariaDB.
